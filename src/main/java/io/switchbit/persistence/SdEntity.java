package io.switchbit.persistence;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

import static io.switchbit.persistence.SdEntity.NAME;

// import static io.switchbit.persistence.OracleXmlDialect.XMLTYPE;

@Entity(name = NAME)
public class SdEntity {

    public static final String NAME = "sd";
    public static final String SEQUENCE_NAME = NAME + "_seq";

    @Id
    // @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id = UUID.randomUUID();

//    @Type(type = XMLTYPE)
    @Type(type = XmlAsObjectUserType.NAME)
    @Column(name = "doc")
    private Object doc;

    SdEntity() {
        // for JPA
    }
    public SdEntity(final Object doc) {
        this.doc = doc;
    }

    public UUID getId() {
        return id;
    }

    public Object getDoc() {
        return doc;
    }
}
