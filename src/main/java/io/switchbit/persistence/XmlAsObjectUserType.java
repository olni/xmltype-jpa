package io.switchbit.persistence;

public class XmlAsObjectUserType extends CustomUserType<Object> {

    public final static String NAME = "io.switchbit.persistence.XmlAsObjectUserType";

    public XmlAsObjectUserType(Class<Object> clazz) {
        super(clazz);
    }

    public XmlAsObjectUserType() {
        super(Object.class);
    }
}
